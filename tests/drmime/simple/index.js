const { test } = require("$TEST_HELPER");

var { readContents, readMulti, findBoundary } = require("$PROJECT/drmime.js");

function throws(fn) {
  try {
    fn();
  } catch (ex) {
    return true;
  }
  return false;
}

test(() => {
  let source = "foo bar";

  test.is(findBoundary(source), -1);
  throws(() => readMulti(source));

  let result = readContents(source);
  test.ok(!!result);
  test.is(result.frontMatter, null);
  test.is(result.mainText.origin, 0);
  test.is(result.mainText.length, source.length);
  test.is(String(result.mainText), source);
  test.is(result.mainText.source, source);
});

// Illegal for an empty line to appear after header start boundary

test(() => {
  let source =
           "---"
  + "\n"
  + "\n" + "foo: bar"
  + "\n" + "---"
  + "\n";

  test.is(findBoundary(source), 0);
  throws(() => readMulti(source));

  let result = readContents(source);
  test.ok(!!result);
  test.is(result.frontMatter, null)
  test.is(result.mainText.origin, 0);
  test.is(result.mainText.length, source.length);
  test.is(String(result.mainText), source);
  test.is(result.mainText.source, source);
});

// Back to testing "normal" records

test(() => {
  let source =
           "---"
  + "\n" + "foo: bar"
  + "\n" + "---"
  + "\n"
  + "\n";

  let textStart = source.length;

  let text =
           "baz"
  + "\n";
  source += text;

  test.is(findBoundary(source), 0);
  test.is(readMulti(source).length, 1);

  let result = readContents(source);
  test.ok(!!result);
  test.is(result.frontMatter.get("foo"), "bar")
  test.is(result.mainText.origin, textStart);
  test.is(result.mainText.length, text.length);
  test.is(String(result.mainText), text);
  test.is(result.mainText.source, source);
});

// Multiple headers

test(() => {
  let source =
           "---"
  + "\n" + "foo: bar"
  + "\n" + "bar: baz"
  + "\n" + "---"
  + "\n";

  let textStart = source.length;

  let text =
           "fum"
  + "\n";
  source += text;

  test.is(findBoundary(source), 0);
  test.is(readMulti(source).length, 1);

  let result = readContents(source);
  test.ok(!!result);
  test.is(result.frontMatter.get("foo"), "bar")
  test.is(result.frontMatter.get("bar"), "baz")
  test.is(result.mainText.origin, textStart);
  test.is(result.mainText.length, text.length);
  test.is(String(result.mainText), text);
  test.is(result.mainText.source, source);
});

// We'll vary the number of lines separating the headers from the body

// First, blank lines

test(() => {
  let source =
           "---"
  + "\n" + "foo: bar"
  + "\n" + "---"
  + "\n";

  let textStart = source.length;

  let text =
           "baz"
  + "\n";
  source += text;

  test.is(findBoundary(source), 0);
  test.is(readMulti(source).length, 1);

  let result = readContents(source);
  test.ok(!!result);
  test.is(result.frontMatter.get("foo"), "bar")
  test.is(result.mainText.origin, textStart);
  test.is(result.mainText.length, text.length);
  test.is(String(result.mainText), text);
  test.is(result.mainText.source, source);
});

// Now two and then three blank lines of separation

test(() => {
  let source =
           "---"
  + "\n" + "foo: bar"
  + "\n" + "---"
  + "\n"
  + "\n"
  + "\n";

  let textStart = source.length;

  let text =
           "baz"
  + "\n";
  source += text;

  test.is(findBoundary(source), 0);
  test.is(readMulti(source).length, 1);

  let result = readContents(source);
  test.ok(!!result);
  test.is(result.frontMatter.get("foo"), "bar")
  test.is(result.mainText.origin, textStart);
  test.is(result.mainText.length, text.length);
  test.is(String(result.mainText), text);
  test.is(result.mainText.source, source);
});

test(() => {
  let source =
           "---"
  + "\n" + "foo: bar"
  + "\n" + "---"
  + "\n"
  + "\n"
  + "\n"
  + "\n";

  let textStart = source.length;

  let text =
           "baz"
  + "\n";
  source += text;

  test.is(findBoundary(source), 0);
  test.is(readMulti(source).length, 1);

  let result = readContents(source);
  test.ok(!!result);
  test.is(result.frontMatter.get("foo"), "bar")
  test.is(result.mainText.origin, textStart);
  test.is(result.mainText.length, text.length);
  test.is(String(result.mainText), text);
  test.is(result.mainText.source, source);
});

// zero-length body

test(() => {
  let source =
           "---"
  + "\n" + "foo: bar"
  + "\n" + "---"
  ;

  let textStart = source.length;

  test.is(readMulti(source).length, 1);

  let result = readContents(source);
  test.ok(!!result);
  test.is(result.frontMatter.get("foo"), "bar")
  test.is(result.mainText.origin, textStart);
  test.is(result.mainText.length, 0);
  test.is(String(result.mainText), "");
  test.is(result.mainText.source, source);
});

test(() => {
  let source =
           "---"
  + "\n" + "foo: bar"
  + "\n" + "---"
  + "\n";

  let textStart = source.length;

  test.is(findBoundary(source), 0);
  test.is(readMulti(source).length, 1);

  let result = readContents(source);
  test.ok(!!result);
  test.is(result.frontMatter.get("foo"), "bar")
  test.is(result.mainText.origin, textStart);
  test.is(result.mainText.length, 0);
  test.is(String(result.mainText), "");
  test.is(result.mainText.source, source);
});

test(() => {
  let source =
           "---"
  + "\n" + "foo: bar"
  + "\n" + "---"
  + "\n"
  + "\n"
  + "\n"
  + "\n";

  let textStart = source.length;

  test.is(findBoundary(source), 0);
  test.is(readMulti(source).length, 1);

  let result = readContents(source);
  test.ok(!!result);
  test.is(result.frontMatter.get("foo"), "bar")
  test.is(result.mainText.origin, textStart);
  test.is(result.mainText.length, 0);
  test.is(String(result.mainText), "");
  test.is(result.mainText.source, source);
});

// No such thing as delimited zero-length headers; must treat it as text

test(() => {
  let source =
           "---"
  + "\n" + "---"
  + "\n";

  test.is(findBoundary(source), 0);
  throws(() => readMulti(source));

  let result = readContents(source);
  test.ok(!!result);
  test.is(result.frontMatter, null);
  test.is(result.mainText.origin, 0);
  test.is(result.mainText.length, source.length);
  test.is(String(result.mainText), source);
  test.is(result.mainText.source, source);
});
// */
