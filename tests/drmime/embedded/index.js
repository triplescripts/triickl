const { test } = require("$TEST_HELPER");

var { readMulti, findBoundary } = require("$PROJECT/drmime.js");

function throws(fn) {
  try {
    fn();
  } catch (ex) {
    return true;
  }
  return false;
}

test(() => {
  let source =
           "<html>"
  + "\n" + "  <head>"
  + "\n" + "    <script>"
  + "\n" + "      /* mesh-lib implementation goes here */"
  + "\n" + "    </script>"
  + "\n" + "    <script>"
  + "\n" + "      Mesh.start = function() {"
  + "\n" + "/* //"
  + "\n" + "---"
  + "\n" + "Content-Type: application/javascript+mesh; version=0.1.4"
  + "\n" + "Content-Discard: first-line, last-line"
  + "\n" + "---"
  + "\n" + "// */"
  + "\n"
  + "\n" + "'use strict';"
  + "\n"
  + "\n" + "// Put your Mesh.attach code in these brackets"
  + "\n" + "// if you need it to run without Mesh"
  + "\n" + "if (require.main === module && typeof Mesh !== 'undefined') {"
  + "\n" + "}"
  + "\n"
  + "\n" + "const foo = ['bar', 'baz'];"
  + "\n" + "Mesh.attach('foo', foo, [0,0]);"
  + "\n" + "/* Discard this line; it's an incomplete comment. //"
  + "\n" + "---"
  + "\n" + "// And that marks the end of the mesh data fork. */"
  + "\n" + "      };"
  + "\n" + "    </script>"
  + "\n" + "  </head>"
  + "\n" + "  <body>"
  + "\n" + "    <!-- ... -->"
  + "\n" + "  </body>"
  + "\n" + "</html>"
  + "\n";

  throws(() => readMulti(source));
  throws(() => readMulti(source, findBoundary(source)));

  let [ result ] = readMulti(source, findBoundary(source), 1);
  test.ok(!!result);
  test.ok(result.frontMatter);
  test.is(result.frontMatter.get("Content-Type"), "application/javascript+mesh; version=0.1.4");
  test.is(result.frontMatter.get("Content-Discard"), "first-line, last-line");
  test.ok(String(result.mainText) !== source);
  test.ok(result.mainText.origin > 0);
  test.ok(result.mainText.length > 0);
  test.ok(result.mainText.origin + result.mainText.length < source.length);
  test.is(result.mainText.source, source);
});
