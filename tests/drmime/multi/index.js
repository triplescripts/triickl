const { test } = require("$TEST_HELPER");

var { readContents, readMulti, findBoundary } = require("$PROJECT/drmime.js");

function throws(fn) {
  try {
    fn();
  } catch (ex) {
    return true;
  }
  return false;
}

function equals(info1, info2) {
  test.is(info1.mainText.origin, info2.mainText.origin);
  test.is(info1.mainText.length, info2.mainText.length);
  test.is(info1.mainText.source, info2.mainText.source);

  let f1 = info1.frontMatter || new Map();
  let f2 = info2.frontMatter || new Map();
  test.is(Array.from(f1.keys()).join(","), Array.from(f2.keys()).join(","));
  for (let key of f1.keys()) {
    if (f1.get(key) !== f2.get(key)) return false;
  }
  return true;
}


// No such thing as delimited zero-length headers; must treat it as text

test(() => {
  let source =
           "---"
  + "\n" + "---"
  + "\n"
  + "\n" + "---"
  + "\n";

  throws(() => readMulti(source));
  throws(() => readMulti(source, 0, 1));

  let result = readContents(source);
  test.ok(!!result);
  test.is(result.frontMatter, null);
  test.is(result.mainText.origin, 0);
  test.is(result.mainText.length, source.length);
  test.is(String(result.mainText), source);
  test.is(result.mainText.source, source);
});

test(() => {
  let source =
           "---"
  + "\n" + "foo: bar"
  + "\n" + "---"
  + "\n"
  + "\n";
  let text =
           "---"
  + "\n";

  let textStart = source.length;
  source += text;

  throws(() => readMulti(source));

  let [ result ] = readMulti(source, 0, 1);
  test.ok(!!result);
  test.is(result.frontMatter.get("foo"), "bar");
  test.is(result.mainText.origin, textStart);
  test.is(result.mainText.length, 0);
  test.is(String(result.mainText), "");
  test.is(result.mainText.source, source);

  result = readContents(source);
  test.ok(!!result);
  test.is(result.frontMatter.get("foo"), "bar");
  test.is(result.mainText.origin, textStart);
  test.is(result.mainText.length, text.length);
  test.is(String(result.mainText), text);
  test.is(result.mainText.source, source);
});

test(() => {
  let source =
           "---"
  + "\n" + "foo: bar"
  + "\n" + "---"
  + "\n"
  + "\n";
  let text =
           "---"
  + "\n" + "---"
  + "\n";

  let textStart = source.length;
  source += text;

  throws(() => readMulti(source));
  throws(() => readMulti(source, 0, 2));
  throws(() => readMulti(source, 0, 3));

  let [ result ] = readMulti(source, 0, 1);
  test.ok(!!result);
  test.is(result.frontMatter.get("foo"), "bar");
  test.is(result.mainText.origin, textStart);
  test.is(result.mainText.length, 0);
  test.is(String(result.mainText), "");
  test.is(result.mainText.source, source);

  result = readContents(source);
  test.ok(!!result);
  test.is(result.frontMatter.get("foo"), "bar");
  test.is(result.mainText.origin, textStart);
  test.is(result.mainText.length, text.length);
  test.is(String(result.mainText), text);
  test.is(result.mainText.source, source);
});

test(() => {
  let source =
           "---"
  + "\n" + "foo: bar"
  + "\n" + "---"
  + "\n"
  + "\n";
  let text =
           "---"
  + "\n"
  + "\n" + "---"
  + "\n";

  let textStart = source.length;
  source += text;

  throws(() => readMulti(source));
  throws(() => readMulti(source, 0, 2));
  throws(() => readMulti(source, 0, 3));

  let [ result ] = readMulti(source, 0, 1);
  test.ok(!!result);
  test.is(result.frontMatter.get("foo"), "bar");
  test.is(result.mainText.origin, textStart);
  test.is(result.mainText.length, 0);
  test.is(String(result.mainText), "");
  test.is(result.mainText.source, source);

  result = readContents(source);
  test.ok(!!result);
  test.is(result.frontMatter.get("foo"), "bar");
  test.is(result.mainText.origin, textStart);
  test.is(result.mainText.length, text.length);
  test.is(String(result.mainText), text);
  test.is(result.mainText.source, source);
});

test(() => {
  let source =
           "---"
  + "\n" + "foo: bar"
  + "\n" + "---"
  + "\n"
  + "\n";
  let text =
           "---"
  + "\n"
  + "\n"
  + "\n" + "---"
  + "\n";

  let textStart = source.length;
  source += text;

  throws(() => readMulti(source));
  throws(() => readMulti(source, 0, 2));
  throws(() => readMulti(source, 0, 3));

  let [ result ] = readMulti(source, 0, 1);
  test.ok(!!result);
  test.is(result.frontMatter.get("foo"), "bar");
  test.is(result.mainText.origin, textStart);
  test.is(result.mainText.length, 0);
  test.is(String(result.mainText), "");
  test.is(result.mainText.source, source);

  result = readContents(source);
  test.ok(!!result);
  test.is(result.frontMatter.get("foo"), "bar");
  test.is(result.mainText.origin, textStart);
  test.is(result.mainText.length, text.length);
  test.is(String(result.mainText), text);
  test.is(result.mainText.source, source);
});

test(() => {
  let source =
           "---"
  + "\n" + "foo: bar"
  + "\n" + "---"
  + "\n"
  + "\n";
  let text =
           "---"
  + "\n" + "foo: baz"
  + "\n" + "---"
  + "\n";

  let textStart = source.length;
  source += text;

  test.is(readMulti(source, 0, 1).length, 1);
  test.is(readMulti(source, 0, 2).length, 2);
  test.is(readMulti(source, 0, 3).length, 2);
  test.is(readMulti(source, 0, 4).length, 2);

  test.is(findBoundary(source), 0);
  test.is(findBoundary(source, textStart), textStart);
  test.is(findBoundary(source, textStart - 1), textStart);
  test.is(findBoundary(source, textStart - 2), textStart);
  test.is(findBoundary(source, textStart - 3), textStart);

  let result = readMulti(source);
  test.is(result.length, 2);
  test.ok(result[0].frontMatter.size > 0);
  test.is(result[0].frontMatter.get("foo"), "bar");
  test.is(result[0].mainText.origin, textStart);
  test.is(result[0].mainText.length, 0);
  test.ok(result[1].frontMatter.size > 0);
  test.is(result[1].frontMatter.get("foo"), "baz");
  test.is(result[1].mainText.origin, source.length);
  test.is(result[1].mainText.length, 0);
  result = result.concat(...readMulti(source, 0, 2))
  test.ok(equals(result[0], result[2]));
  test.ok(equals(result[0], ...readMulti(source, 0, 1)));
  test.ok(equals(result[1], result[3]));
  test.ok(equals(result[1], ...readMulti(source, textStart, 1)));

  result = readContents(source);
  test.ok(!!result);
  test.is(result.frontMatter.get("foo"), "bar");
  test.is(result.mainText.origin, textStart);
  test.is(result.mainText.length, text.length);
  test.is(String(result.mainText), text);
  test.is(result.mainText.source, source);
});

test(() => {
  let source =
           "---"
  + "\n" + "foo: bar"
  + "\n" + "---"
  + "\n"
  + "\n";
  let text =
           "---"
  + "\n"
  + "\n"
  + "\n" + "---"
  + "\n" + "foo: baz"
  + "\n" + "---"
  + "\n";

  let textStart = source.length;
  source += text;

  throws(() => readMulti(source));
  test.is(findBoundary(source, textStart), textStart);
  throws(() => readMulti(source, 0, 2));

  test.is(readMulti(source, 0, 1).length, 1);
  test.is(readMulti(source, findBoundary(source, textStart + 1)).length, 1);

  let result = readContents(source);
  test.ok(!!result);
  test.is(result.frontMatter.get("foo"), "bar");
  test.is(result.mainText.origin, textStart);
  test.is(result.mainText.length, text.length);
  test.is(String(result.mainText), text);
  test.is(result.mainText.source, source);
});

test(() => {
  let source =
           "---"
  + "\n" + "foo: bar"
  + "\n" + "---"
  + "\n"
  + "\n";
  let text =
           "Paragraphs."
  + "\n"
  + "\n" + "And paragraphs."
  + "\n"
  + "\n" + "Of text."
  + "\n"
  + "\n" + "---"
  + "\n"
  + "\n" + "*Addendum*: I've since revised my stance on this issue"
  + "\n";

  let textStart = source.length;
  source += text;

  throws(() => readMulti(source));

  test.ok(findBoundary(source, textStart) > textStart);
  test.ok(findBoundary(source, textStart) < source.length);

  let result = readContents(source);
  test.ok(!!result);
  test.is(result.frontMatter.get("foo"), "bar");
  test.is(result.mainText.origin, textStart);
  test.is(result.mainText.length, text.length);
  test.is(String(result.mainText), text);
  test.is(result.mainText.source, source);
});
