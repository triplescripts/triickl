import { read, test } from "$TEST_HELPER";

import { DrMime } from "$PROJECT/src/DrMime.src";

function $require(id) {
  if (id == "$PROJECT/drmime.js") return DrMime;
  return require(id);
}

function sub(path) {
  return read(path).then((source) => {
    (new Function("require", source))($require);
  });
}

test(() => {
  return Promise.all([
    sub("$PROJECT/tests/drmime/simple/index.js"),
    sub("$PROJECT/tests/drmime/multi/index.js"),
    sub("$PROJECT/tests/drmime/embedded/index.js")
  ]);
});
