import { test } from "$TEST_HELPER";

import { HashUtils } from "$PROJECT/src/HashUtils.src";

const sha1 = HashUtils.init();

test(() => {
  test.is("743f9f64b1e2a6d3593a7b769f96d9425639a10d", sha1("Hey, hash this"));
  test.is("5bb5e3ae259f24abd7c975809c9b6393c80dfb3f", sha1("and this, pls."));
})
