import { test } from "$TEST_HELPER";

import { TriicklConfig } from "$PROJECT/src/TriicklConfig.src";

const DEFAULT_VALUES = TriicklConfig.makeDefaults();

test(function simple_single() {
  let text =
         "posts: _statuses" +
  "\n";
  let config = TriicklConfig.from(text);
  test.is(config.get("posts"), "_statuses");
})

test(function simple_multiple() {
  let text =
         "name: Blog" +
  "\n" + "subtitle: Thoughts" +
  "\n";
  let config = TriicklConfig.from(text);
  test.is(config.get("name"), "Blog");
  test.is(config.get("subtitle"), "Thoughts");
})

test(function multi_word_values() {
  let text =
         "name: My Blog" +
  "\n" + "subtitle: Some of my thoughts" +
  "\n";
  let config = TriicklConfig.from(text);
  test.is(config.get("name"), "My Blog");
  test.is(config.get("subtitle"), "Some of my thoughts");
})

test(function quoted_values() {
  let text =
         "name: \"My Blog\"" +
  "\n" + "subtitle: 'Some of my thoughts'" +
  "\n";
  let config = TriicklConfig.from(text);
  test.is(config.get("name"), "My Blog");
  test.is(config.get("subtitle"), "'Some of my thoughts'");
})

test(function tchars() {
  let text =
         "foo-bar: baz" +
  "\n" + "fum_nom: foo" +
  "\n";
  let config = TriicklConfig.from(text);
  test.is(config.get("foo-bar"), "baz");
  test.is(config.get("fum_nom"), "foo");
})

test(function defaults() {
  let layoutName = "foo";
  let text =
         "default_layout: " + layoutName +
  "\n";

  let config = TriicklConfig.from(text);
  test.ok(layoutName != DEFAULT_VALUES.default_layout);
  test.is(config.get("default_layout"), layoutName);
  test.is(config.get("posts"), DEFAULT_VALUES.posts);
})
