triickl
=======

triickl is a self-contained static site generator.

Instructions
============

Build triickl by using `Build.app.htm` (included in the repo) to run the
following command:

    build main.src -o triickl.app.htm

If you prefer to stay in the terminal, rather than launching the build tool's
graphical interface, then you can run `Build.app.htm` using GraalVM or NodeJS:

    node ./Build.app.htm $BUILDCOMMAND

(... where `$BUILDCOMMAND` is the one listed above).

You can then use `triickl.app.htm` on your site's source directory to generate
your static site from its markdown/HTML/XML/etc sources.  There is support for
jekyll-like layouts.  They use the syntax from the Mold templating engine.

License
=======

This work is made available under the terms of the MIT License.

Copyright individual authors and contributors; see ACKNOWLEDGEMENTS.txt.
